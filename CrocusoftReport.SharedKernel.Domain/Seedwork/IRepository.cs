﻿using System.Linq.Expressions;

namespace CrocusoftReport.SharedKernel.Domain.Seedwork
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
        Task<T> AddAsync(T entity);
        Task<T> GetByIdAsync(int id);
        Task<List<T>> GetAllAsync();
        T Get(Expression<Func<T, bool>> exp, params string[] includes);
        T Update(T entity);
        bool Delete(T entity);
    }
}