﻿namespace CrocusoftReport.ReportDetails.ViewModels
{
    public class AllReportDto
    {
        public List<ReportDto> Reports { get; set; }
        public int TotalCount { get; set; }
    }
}