﻿using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.ReportDetails.ViewModels
{
    public class ReportDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string Note { get; set; }
        public string CreatedDate { get; set; }
        public UserInfoDto User { get; set; }
    }
}