﻿using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.ReportDetails.ViewModels;

namespace CrocusoftReport.ReportDetails.ReportProfiles
{
    public class ReportProfile : Profile
    {
        public ReportProfile()
        {
            CreateMap<Report, ReportDto>()
                .ForMember(dest => dest.User, opt => opt
                .MapFrom(src => src.CreatedBy))

                .ForMember(dest => dest.ProjectName, opt => opt.
                MapFrom(src => src.Project.ProjectName))

            .ForMember(dest => dest.CreatedDate, opt => opt.
                MapFrom(src => src.RecordDate.Date.ToString("dd-MM-yyyy")))

                .ForMember(dest => dest.CreatedDate, opt => opt.
                MapFrom(src => src.RecordDate.Date.ToString("dd-MM-yyyy")))

                .ForMember(dest => dest.ProjectName, opt => opt.
                MapFrom(src => src.Project.ProjectName));
        }
    }
}