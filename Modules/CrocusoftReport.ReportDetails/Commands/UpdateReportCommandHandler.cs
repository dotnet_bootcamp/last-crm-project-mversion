﻿using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class UpdateReportCommandHandler : IRequestHandler<UpdateReportCommand, bool>
    {
        private readonly IReportRepository _reportRepository;
        private readonly IUserManager _userManager;
        public UpdateReportCommandHandler(IReportRepository reportRepository, IUserManager userManager)
        {
            _reportRepository = reportRepository;
            _userManager = userManager;
        }
        public async Task<bool> Handle(UpdateReportCommand request, CancellationToken cancellationToken)
        {
            var userId = _userManager.GetCurrentUserId();
            Report report = await _reportRepository.GetByIdAsync(request.Id);

            if (report.CreatedById == userId && report.RecordDate.Date == DateTime.UtcNow.AddHours(4).Date)
            {
                report.SetEditFields(userId);
            }

            else
            {
                throw new UnauthorizedAccessException("You are not allowed to update this report.");
            }

            report.SetDetails(request.Note, report.ProjectId);
            _reportRepository.Update(report);
            await _reportRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class RegisterReportIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateReportCommand, bool>
    {
        public RegisterReportIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}