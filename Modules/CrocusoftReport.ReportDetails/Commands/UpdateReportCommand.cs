﻿using MediatR;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class UpdateReportCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string Note { get; set; }
    }
}