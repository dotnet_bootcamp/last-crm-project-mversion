﻿using FluentValidation;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class CreateReportCommandValidator : AbstractValidator<CreateReportCommand>
    {
        public CreateReportCommandValidator() : base()
        {
            RuleFor(r => r.ProjectId).NotNull();
            RuleFor(r => r.Note).NotNull().NotEmpty().MaximumLength(250);
        }
    }
}