﻿using FluentValidation;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class UpdateReportCommandValidator : AbstractValidator<UpdateReportCommand>
    {
        public UpdateReportCommandValidator() : base()
        {
            RuleFor(r => r.Id).NotNull();
            RuleFor(r => r.Note).NotNull().NotEmpty().MaximumLength(500);
        }
    }
}