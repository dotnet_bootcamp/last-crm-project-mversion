﻿using MediatR;

namespace CrocusoftReport.ReportDetails.Commands
{
    public class CreateReportCommand : IRequest<bool>
    {
        public int ProjectId { get; set; }
        public string Note { get; set; }
    }
}