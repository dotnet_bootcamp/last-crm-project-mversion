﻿using AutoMapper;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.ReportDetails.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace CrocusoftReport.ReportDetails.Queries
{
    public class ReportQueries : IReportQueries
    {
        private readonly CrocusoftReportDbContext _context;
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;
        public ReportQueries(CrocusoftReportDbContext context, IMapper mapper, IUserManager userManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<AllReportDto> GetAllUserAsync(ReportRequestForUserDto request, LoadMoreDto dto)
        {
            var user = await _userManager.GetCurrentUser();

            DateTime? startDate = request.StartDate;
            DateTime? endDate = request.EndDate;

            var entities = _context.Reports.Include(m => m.Project)
            .Include(m => m.CreatedBy).ThenInclude(p => p.ProjectUsers)
            .Where(m => m.CreatedBy.Id == user.Id &&
            (request.ProjectIds == null || request.ProjectIds.Any() ||
            m.Project != null && request.ProjectIds.Contains(m.Project.Id)) &&
             (startDate == null || m.RecordDate.Date >= startDate) &&
             (endDate == null || m.RecordDate.Date <= endDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59))
            )
           .OrderByDescending(p => p.Id).AsQueryable();

            var count = (await entities.ToListAsync()).Count;

            if (dto.SortField != null)
            {
                entities = dto.OrderBy
                    ? entities.OrderBy($"p=>p.{dto.SortField}")
                    : entities.OrderBy($"p=>p.{dto.SortField} descending");
            }

            if (dto.Skip != null && dto.Take != null)
            {
                entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
            }

            var reportModel = new List<ReportDto>();

            foreach (var report in entities)
            {
                var reportDto = _mapper.Map<ReportDto>(report);
                reportModel.Add(reportDto);
            }

            AllReportDto outputModel = new()
            {
                Reports = reportModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<AllReportDto> GetAllAdminAsync(ReportRequestForAdminDto request, LoadMoreDto dto)
        {
            DateTime? startDate = request.StartDate;
            DateTime? endDate = request.EndDate;

            var entities = _context.Reports.Include(m => m.Project)
             .Include(m => m.CreatedBy).ThenInclude(p => p.ProjectUsers)
             .Where(m =>
             (request.ProjectIds == null ||
             (m.Project != null && request.ProjectIds.Contains(m.Project.Id))) &&
             (request.UserIds == null ||
             (m.CreatedBy != null && request.UserIds.Contains(m.CreatedBy.Id))) &&
              (startDate == null || m.RecordDate.Date >= startDate) &&
              (endDate == null || m.RecordDate.Date <= endDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59)))
             .OrderByDescending(p => p.Id).AsQueryable();

            var count = (await entities.ToListAsync()).Count;

            if (dto.SortField != null)
            {
                entities = dto.OrderBy
                    ? entities.OrderBy($"p=>p.{dto.SortField}")
                    : entities.OrderBy($"p=>p.{dto.SortField} descending");
            }

            if (dto.Skip != null && dto.Take != null)
            {
                entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
            }

            var reportModel = new List<ReportDto>();

            foreach (var report in entities)
            {
                var reportDto = _mapper.Map<ReportDto>(report);
                reportModel.Add(reportDto);
            }

            AllReportDto outputModel = new()
            {
                Reports = reportModel,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<ReportDto> GetByIdAsync(int id)
        {
            var report = await _context.Reports
                .Include(m => m.Project)
                .Include(m => m.CreatedBy)
                .FirstOrDefaultAsync(u => u.Id == id);
            var dto = _mapper.Map<ReportDto>(report);
            return dto;
        }
    }
}