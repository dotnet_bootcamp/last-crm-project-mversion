﻿using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.ProjectDetails.ViewModels
{
    public class ProjectGetByIdDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public List<UserInfoDto> Users { get; set; }
    }
}