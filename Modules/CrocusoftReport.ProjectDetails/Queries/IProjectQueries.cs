﻿using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.ProjectDetails.ViewModels;
using CrocusoftReport.SharedKernel.Infrastructure.Queries;

namespace CrocusoftReport.ProjectDetails.Queries
{
    public interface IProjectQueries : IQuery
    {
        Task<ProjectGetByIdDto> GetByIdAsync(int projectId);
        Task<AllProjectDto> GetAllAsync(ProjectRequestDto request, LoadMoreDto dto);
    }
}