﻿using FluentValidation;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class UpdateProjectCommandValidator : AbstractValidator<UpdateProjectCommand>
    {
        public UpdateProjectCommandValidator() : base()
        {
            RuleFor(p => p.Id).NotNull().NotEmpty();
            RuleFor(p => p.ProjectName).NotNull().NotEmpty();
        }
    }
}