﻿using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.ProjectDetails.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, bool>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly CrocusoftReportDbContext _context;
        public UpdateProjectCommandHandler(IProjectRepository projectRepository, CrocusoftReportDbContext context)
        {
            _projectRepository = projectRepository;
            _context = context;
        }
        public async Task<bool> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            Project? existingProject = await _context.Projects
                .FirstOrDefaultAsync(t => t.ProjectName == request.ProjectName && t.Id != request.Id,
                cancellationToken: cancellationToken);

            if (existingProject is not null)
            {
                throw new DomainException($"Project already exists, please choose another name.");
            }

            var project = await _projectRepository.GetByIdAsync(request.Id);

            if (request.UserIds != null && request.UserIds.Any())
            {
                var existingProjects = await _context.UserProjects.Where(up => up.ProjectId == project.Id)
                    .ToListAsync(cancellationToken: cancellationToken);

                foreach (var userId in request.UserIds)
                {
                    var projectUser = existingProjects.FirstOrDefault(x => x.UserId == userId);
                    if (projectUser is null)
                    {
                        var newProjectUser = new ProjectUser(userId, project.Id);
                        _context.UserProjects.Add(newProjectUser);
                    }
                }

                foreach (var userProject in existingProjects)
                {
                    if (!request.UserIds.Contains(userProject.UserId))
                    {
                        _context.UserProjects.Remove(userProject);
                    }
                }
            }
            project.SetDetails(request.ProjectName.Trim());
            _projectRepository.Update(project);
            await _projectRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
public class RegisterProjectIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateProjectCommand, bool>
{
    public RegisterProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
    {
    }
    protected override bool CreateResultForDuplicateRequest()
    {
        return true;
    }
}