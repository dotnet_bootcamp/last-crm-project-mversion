﻿using MediatR;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class CreateProjectCommand : IRequest<bool>
    {
        public string ProjectName { get; set; }
        public List<int>? UserIds { get; set; }
    }
}