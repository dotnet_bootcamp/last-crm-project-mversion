﻿using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.ProjectDetails.Commands
{
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, bool>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly CrocusoftReportDbContext _context;
        public CreateProjectCommandHandler(IProjectRepository projectRepository, CrocusoftReportDbContext context)
        {
            _projectRepository = projectRepository;
            _context = context;
        }
        public async Task<bool> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            Project? existingProject = await _context.Projects
                .FirstOrDefaultAsync(t => t.ProjectName == request.ProjectName, cancellationToken: cancellationToken);

            if (existingProject is not null)
            {
                throw new DomainException($"Project already exists, please choose another name.");
            }

            var newProject = new Project();
            newProject.SetDetails(request.ProjectName);

            if (request.UserIds != null && request.UserIds.Any())
            {
                var users = await _context.Users.Include(m => m.Role).Where(u => request.UserIds
                .Contains(u.Id)).ToListAsync(cancellationToken);

                if (users.Any(x => x.Role.Name != CustomRole.Employee))
                {
                    throw new DomainException("You can choose only employee");
                }

                newProject.AddUsers(users);
            }

            await _projectRepository.AddAsync(newProject);
            await _projectRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return true;
        }
    }
    public class CreateProjectIdentifiedCommandHandler : IdentifiedCommandHandler<CreateProjectCommand, bool>
    {
        public CreateProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}