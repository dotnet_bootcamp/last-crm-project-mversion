﻿using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.ProjectDetails.ViewModels;

namespace CrocusoftReport.ProjectDetails.ProjectProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<Project, ProjectGetByIdDto>()
                .ForMember(dest => dest.Users, opt => opt
                .MapFrom(src => src.ProjectUsers
                .Select(up => up.User)));
           
        }
    }
}