﻿using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.TeamDetails.ViewModels
{
    public class TeamGetByIdDto
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public List<UserInfoDto> Users { get; set; }
    }
}