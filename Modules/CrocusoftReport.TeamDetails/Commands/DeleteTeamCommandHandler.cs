﻿using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using MediatR;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamCommand, bool>
    {
        private readonly ITeamRepository _teamRepository;
        public DeleteTeamCommandHandler(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }
        public async Task<bool> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            foreach (int teamId in request.Ids)
            {
                var team = _teamRepository.Get(x => x.Id == teamId, "Users");
                if (team is not null)
                {
                    if (team.Users != null && team.Users.Any())
                    {
                        throw new DomainException($"Team '{team.TeamName}' cannot be deleted as it contains employees.");
                    }
                    _teamRepository.Delete(team);
                }
            }
            await _teamRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class DeleteTeamIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteTeamCommand, bool>
    {
        public DeleteTeamIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}