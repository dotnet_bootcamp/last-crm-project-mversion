﻿using MediatR;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class DeleteTeamCommand : IRequest<bool>
    {
        public List<int> Ids { get; set; }
    }
}