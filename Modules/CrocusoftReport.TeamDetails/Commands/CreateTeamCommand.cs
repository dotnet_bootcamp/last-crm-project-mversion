﻿using MediatR;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class CreateTeamCommand : IRequest<bool>
    {
        public string TeamName { get; set; }
    }
}