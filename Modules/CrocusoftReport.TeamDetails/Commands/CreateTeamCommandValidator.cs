﻿using FluentValidation;

namespace CrocusoftReport.TeamDetails.Commands
{
    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator() : base()
        {
            RuleFor(t => t.TeamName).NotEmpty().NotNull();
        }
    }
}