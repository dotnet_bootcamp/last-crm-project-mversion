﻿using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.SharedKernel.Infrastructure.Queries;
using CrocusoftReport.TeamDetails.ViewModels;

namespace CrocusoftReport.TeamDetails.Queries
{
    public interface ITeamQueries : IQuery
    {
        Task<AllTeamDto> GetAllAsync(LoadMoreDto loadMore);
        Task<TeamGetByIdDto> GetByIdAsync(int id);
    }
}