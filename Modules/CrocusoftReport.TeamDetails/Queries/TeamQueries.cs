﻿using AutoMapper;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.TeamDetails.ViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace CrocusoftReport.TeamDetails.Queries
{
    public class TeamQueries : ITeamQueries
    {
        private readonly IMapper _mapper;
        private readonly CrocusoftReportDbContext _context;
        public TeamQueries(CrocusoftReportDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<AllTeamDto> GetAllAsync(LoadMoreDto loadMore)
        {
            var entities = _context.Teams.Include(m => m.Users).OrderByDescending(p => p.Id).AsQueryable();
            var count = (await entities.ToListAsync()).Count;

            if (loadMore.SortField != null)
            {
                entities = loadMore.OrderBy
                    ? entities.OrderBy($"p=>p.{loadMore.SortField}")
                    : entities.OrderBy($"p=>p.{loadMore.SortField} descending");
            }

            if (loadMore.Skip != null && loadMore.Take != null)
            {
                entities = entities.Skip(loadMore.Skip.Value).Take(loadMore.Take.Value);
            }

            var teamModel = _mapper.Map<List<TeamDto>>(entities);
            var outputModel = new AllTeamDto
            {
                Teams = teamModel,
                TotalCount = count
            };
            return outputModel;
        }
        public async Task<TeamGetByIdDto> GetByIdAsync(int id)
        {
            var entity = await _context.Teams
         .Include(m => m.Users.Where(u => !u.IsDeleted))
         .FirstOrDefaultAsync(t => t.Id == id);
            return _mapper.Map<TeamGetByIdDto>(entity);
        }
    }
}