﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ActiveUserCommandHandler : IRequestHandler<ActiveUserCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        private readonly IUserManager _userManager;
        public ActiveUserCommandHandler(IAppUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }
        public async Task<bool> Handle(ActiveUserCommand request, CancellationToken cancellationToken)
        {
            var user = _userRepository.Get(x => x.Id == request.Id, "Role");
            var currentUser = await _userManager.GetCurrentUser();

            if (currentUser.Role.Name == CustomRole.Admin && user.Role.Name == CustomRole.SuperAdmin
                || currentUser.Role.Name == CustomRole.Admin && user.Role.Name == CustomRole.Admin)
            {
                throw new DomainException("You do not have access change activate!");
            }

            user.SetActivated(request.IsActive);
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class ActiveUserIdentifiedCommandHandler : IdentifiedCommandHandler<ActiveUserCommand, bool>
    {
        public ActiveUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}