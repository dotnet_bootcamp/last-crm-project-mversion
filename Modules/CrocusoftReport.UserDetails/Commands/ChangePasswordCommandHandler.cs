﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Infrastructure;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        private readonly IUserManager _userManager;
        public ChangePasswordCommandHandler(IAppUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }
        public async Task<bool> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.GetCurrentUser();
            var oldPasswordHash = PasswordHasher.HashPassword(request.OldPassword);
            var newPasswordHash = PasswordHasher.HashPassword(request.NewPassword);

            if (user.PasswordHash != oldPasswordHash)
            {
                throw new DomainException("Invalid old password");
            }

            if (request.NewPassword != request.NewPasswordConfirm)
            {
                throw new DomainException("New password and confirmation password do not match");
            }

            user.SetPasswordHash(newPasswordHash);
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class ChangePasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ChangePasswordCommand, bool>
    {
        public ChangePasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}