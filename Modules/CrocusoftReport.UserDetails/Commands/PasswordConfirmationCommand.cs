﻿using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class PasswordConfirmationCommand : IRequest<bool>
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
}