﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using CrocusoftReport.SharedKernel.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.UserDetails.Commands
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        private readonly IUserManager _userManager;
        private readonly CrocusoftReportDbContext _context;
        public RegisterUserCommandHandler(IAppUserRepository userRepository, CrocusoftReportDbContext context, IUserManager userManager)
        {
            _userRepository = userRepository;
            _context = context;
            _userManager = userManager;
        }
        public async Task<bool> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var currentUser = await _userManager.GetCurrentUser();
            var role = await _context.Roles.FirstOrDefaultAsync(p => p.Id == request.RoleId, cancellationToken: cancellationToken);

            if (currentUser.Role.Name == CustomRole.SuperAdmin)
            {
                if (role?.Name == CustomRole.SuperAdmin)
                {
                    throw new DomainException("You do not have accesss to create this role!");
                }
            }

            if (currentUser.Role.Name == CustomRole.Admin)
            {
                if (role?.Name == CustomRole.SuperAdmin || role?.Name == CustomRole.Admin)
                {
                    throw new DomainException("You do not have access to create this role!");
                }
            }

            if (!request.Email.EndsWith("@crocusoft.com", StringComparison.OrdinalIgnoreCase))
            {
                throw new DomainException("Invalid email. Only @crocusoft.com email addresses are allowed.");
            }

            AppUser? existingUser = _userRepository.Get(x => x.Email == request.Email);

            if (existingUser is not null)
            {
                throw new DomainException("User already taken, please choose another email.");
            }

            Team? team = await _context.Teams
                .FirstOrDefaultAsync(t => t.Id == request.TeamId,
                cancellationToken: cancellationToken);

            var user = new AppUser(request.Email.Trim(),
                PasswordHasher.HashPassword(request.Password),
                request.FirstName, request.LastName);

            if (role is not null)
            {
                user.SetRole(role.Id);
            }

            if (team is not null)
            {
                user.SetTeam(team.Id);
            }

            user.SetActivated(true);
            await _userRepository.AddAsync(user);
            await _userRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return true;
        }
    }
    public class RegisterUserIdentifiedCommandHandler : IdentifiedCommandHandler<RegisterUserCommand, bool>
    {
        public RegisterUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}