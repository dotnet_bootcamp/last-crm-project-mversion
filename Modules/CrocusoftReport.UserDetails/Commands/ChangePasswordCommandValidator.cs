﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator() : base()
        {
            RuleFor(command => command.OldPassword).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");

            RuleFor(command => command.NewPassword).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");
            RuleFor(command => command.NewPasswordConfirm).NotNull();
        }
    }
}