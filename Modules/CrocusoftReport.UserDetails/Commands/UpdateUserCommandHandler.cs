﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, bool>
    {
        private readonly IAppUserRepository _userRepository;
        private readonly CrocusoftReportDbContext _context;
        private readonly IUserManager _userManager;
        public UpdateUserCommandHandler(IAppUserRepository userRepository, IUserManager userManager, CrocusoftReportDbContext context)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _context = context;
        }
        public async Task<bool> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _userRepository.Get(x => x.Id == request.Id, "Role");
            var currentUser = await _userManager.GetCurrentUser();

            if (currentUser.Role.Name == CustomRole.Admin &&
                (user.Role.Name == CustomRole.SuperAdmin || user.Role.Name == CustomRole.Admin))
            {
                throw new DomainException("You do not have access to update admin or superadmin");
            }

            var existingUser = _userRepository.Get(x => x.Email == request.Email && x.Id != request.Id);

            if (request.Email != user.Email)
            {
                if (existingUser is not null)
                {
                    throw new DomainException($"Email '{request.Email}' already taken, please choose another name.");
                }
            }
            user.SetDetails(request.Email.Trim(), request.FirstName, request.LastName);
            user.SetRole(request.RoleId);

            if (request.TeamId.Value != 0)
            {
                user.SetTeam(request.TeamId);
            }

            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
        public class RegisterUserIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateUserCommand, bool>
        {
            public RegisterUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }
            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}