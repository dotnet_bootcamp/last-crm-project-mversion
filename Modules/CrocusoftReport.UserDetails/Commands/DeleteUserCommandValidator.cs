﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
        }
    }
}