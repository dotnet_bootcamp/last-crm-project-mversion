﻿using CrocusoftReport.Identity.Queries;
using CrocusoftReport.Infrastructure;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.UserDetails.Commands.Models;
using MediatR;
using Microsoft.Extensions.Options;
using System.Security.Authentication;

namespace CrocusoftReport.UserDetails.Commands
{
    public class CheckTokenVerifyInputCommandHandler : IRequestHandler<CheckTokenVerifyInputCommand, bool>
    {
        private readonly IUserQueries _userQueries;
        private readonly CrocusoftReportSettings _settings;
        public CheckTokenVerifyInputCommandHandler(IUserQueries userQueries, IOptions<CrocusoftReportSettings> settings)
        {
            _userQueries = userQueries;
            _settings = settings.Value;
        }
        public async Task<bool> Handle(CheckTokenVerifyInputCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email.ToLower();
            var user = await _userQueries.FindByEmailAsync(email);
            var userId = user.Id.ToString();  //because of validateTokenUserId , it accepts only String value
            if (user == null)
                throw new AuthenticationException("Invalid credentials.");

            var validateTokenUserId = TokenManager.ValidateToken(_settings, request.Token) ?? throw new AuthenticationException("Token is null");

            if (!userId.Equals(validateTokenUserId))
            {
                throw new AuthenticationException("Token is invalid for this user");
            }
            return true;
        }
        public class CheckTokenInputCommandHandler : IdentifiedCommandHandler<CheckTokenVerifyInputCommand, bool>
        {
            public CheckTokenInputCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }
            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}