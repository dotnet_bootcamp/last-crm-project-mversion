﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    class GetAuthorizationTokenCommandValidator : AbstractValidator<GetAuthorizationTokenCommand>
    {
        public GetAuthorizationTokenCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull().EmailAddress();
            RuleFor(command => command.Password).NotNull();
        }
    }
}