﻿using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Commands;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using CrocusoftReport.SharedKernel.Infrastructure;
using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, bool>
    {
        private readonly IUserManager _userManager;
        private readonly IAppUserRepository _userRepository;
        public ResetPasswordCommandHandler(IUserManager userManager, IAppUserRepository userRepository)
        {
            _userManager = userManager;
            _userRepository = userRepository;
        }
        public async Task<bool> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = _userRepository.Get(u => u.Id == request.UserId, "Role");
            var currentUser = await _userManager.GetCurrentUser();

            if ((currentUser.Role.Name == CustomRole.Admin && user.Role.Name == CustomRole.SuperAdmin) ||
                (currentUser.Role.Name == CustomRole.Admin && user.Role.Name == CustomRole.Admin && currentUser != user))
            {
                throw new DomainException("You do not have access to reset this password!");
            }

            if (request.NewPassword != request.NewPasswordConfirm)
            {
                throw new DomainException("New password and confirmation password do not match");
            }

            user.ResetPassword(PasswordHasher.HashPassword(request.NewPassword));
            _userRepository.Update(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class ResetPasswordIdentifiedCommandHandler : IdentifiedCommandHandler<ResetPasswordCommand, bool>
    {
        public ResetPasswordIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }
        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}