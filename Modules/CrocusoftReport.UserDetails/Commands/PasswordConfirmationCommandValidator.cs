﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class PasswordConfirmationCommandValidator : AbstractValidator<PasswordConfirmationCommand>
    {
        public PasswordConfirmationCommandValidator() : base()
        {
            RuleFor(command => command.NewPassword).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");

            RuleFor(command => command.ConfirmNewPassword).NotNull().MinimumLength(8);
        }
    }
}