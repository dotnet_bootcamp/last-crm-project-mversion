﻿using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class DeleteUserCommand : IRequest<bool>
    {
        public List<int> Id { get; set; }
    }
}