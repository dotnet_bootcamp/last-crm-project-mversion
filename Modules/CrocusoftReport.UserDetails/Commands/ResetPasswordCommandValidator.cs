﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        public ResetPasswordCommandValidator() : base()
        {
            RuleFor(command => command.UserId).NotNull();
            RuleFor(command => command.NewPassword).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");

            RuleFor(command => command.NewPasswordConfirm).NotNull().MinimumLength(8)
                .Must(password => password.Any(char.IsUpper))
                .WithMessage("Password must contain at least one uppercase letter.");
        }
    }
}