﻿using MediatR;

namespace CrocusoftReport.UserDetails.Commands
{
    public class ActiveUserCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}