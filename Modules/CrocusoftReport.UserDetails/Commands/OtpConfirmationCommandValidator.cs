﻿using FluentValidation;

namespace CrocusoftReport.UserDetails.Commands
{
    public class OtpConfirmationCommandValidator : AbstractValidator<OtpConfirmationCommand>
    {
        public OtpConfirmationCommandValidator() : base()
        {
            RuleFor(command => command.OtpCode).NotNull();
        }
    }
}