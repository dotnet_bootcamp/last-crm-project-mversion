﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Identity.Queries;
using CrocusoftReport.Infrastructure;
using CrocusoftReport.Infrastructure.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace CrocusoftReport.Identity.Auth
{
    public class UserManager : IUserManager
    {
        private readonly IUserQueries _userQueries;
        private readonly CrocusoftReportSettings _settings;
        private readonly IClaimsManager _claimsManager;

        public UserManager(IUserQueries userQueries, IOptions<CrocusoftReportSettings> settings, IClaimsManager claimsManager)
        {
            _userQueries = userQueries;
            _claimsManager = claimsManager;
            if (settings == null) throw new ArgumentNullException(nameof(settings));
            _settings = settings.Value;
        }
        public int GetCurrentUserId()
        {
            return _claimsManager.GetCurrentUserId();
        }
        public async Task<AppUser> GetCurrentUser()
        {
            var currentUserId = GetCurrentUserId();
            return await _userQueries.FindAsync(currentUserId);
        }
        public (string token, DateTime expiresAt) GenerateJwtToken(AppUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            claims.AddRange(_claimsManager.GetUserClaims(user));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.JwtKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiresAt = DateTime.UtcNow.Date.AddDays(30);

            var token = new JwtSecurityToken(
                _settings.JwtIssuer,
                _settings.JwtIssuer,
                claims,
                expires: expiresAt,
                signingCredentials: creds
            );

            var tokenHandler = new JwtSecurityTokenHandler();
            return (tokenHandler.WriteToken(token), expiresAt);
        }
    }
}