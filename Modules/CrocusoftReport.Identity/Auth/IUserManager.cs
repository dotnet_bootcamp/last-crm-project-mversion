﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;

namespace CrocusoftReport.Identity.Auth
{
    public interface IUserManager
    {
        int GetCurrentUserId();
        Task<AppUser> GetCurrentUser();
        (string token, DateTime expiresAt) GenerateJwtToken(AppUser user);
    }
}