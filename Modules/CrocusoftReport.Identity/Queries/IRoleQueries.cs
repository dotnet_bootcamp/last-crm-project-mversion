﻿using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.SharedKernel.Infrastructure.Queries;

namespace CrocusoftReport.Identity.Queries
{
    public interface IRoleQueries : IQuery
    {
        Task<IEnumerable<RoleDto>> GetAllAsync();
    }
}