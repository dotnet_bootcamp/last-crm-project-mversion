﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.SharedKernel.Infrastructure.Queries;

namespace CrocusoftReport.Identity.Queries
{
    public interface IUserQueries : IQuery
    {
        Task<AppUser> FindByEmailAsync(string email);
        Task<AppUser> FindAsync(int userId);
        Task<UserProfileDto> GetUserProfileAsync(int userId);
        public Task<AllUserDto> GetAllUsersAsync(UserRequestDto request, LoadMoreDto dto);
        public Task<UserGetByIdDto> GetUserById(int id);
    }
}