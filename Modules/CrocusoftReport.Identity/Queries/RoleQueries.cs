﻿using AutoMapper;
using CrocusoftReport.Identity.ViewModels;
using CrocusoftReport.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Identity.Queries
{
    public class RoleQueries : IRoleQueries
    {
        private readonly CrocusoftReportDbContext _context;
        private readonly IMapper _mapper;
        public RoleQueries(CrocusoftReportDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var entities = await _context.Roles.ToListAsync();
            return _mapper.Map<IEnumerable<RoleDto>>(entities);
        }
    }
}