﻿namespace CrocusoftReport.Identity.ViewModels
{
    public class UserInfoDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public bool IsDeleted { get; set; }

    }
}