﻿using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.Identity.ViewModels
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public RoleDto Role { get; set; }
        public TeamInfoDto Team { get; set; }
    }
}