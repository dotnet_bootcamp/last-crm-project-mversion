﻿namespace CrocusoftReport.Identity.ViewModels
{
    public class TeamInfoDto
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
    }
}