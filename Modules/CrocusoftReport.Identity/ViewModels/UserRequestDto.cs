﻿namespace CrocusoftReport.Identity.ViewModels
{
    public class UserRequestDto
    {
        public List<int> TeamIds { get; set; }
        public List<int> ProjectIds { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}