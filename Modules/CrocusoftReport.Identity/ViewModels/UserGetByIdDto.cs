﻿using CrocusoftReport.Identity.ViewModels;

namespace CrocusoftReport.Identity.ViewModels
{
    public class UserGetByIdDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public RoleDto Role { get; set; }
        public TeamInfoDto Team { get; set; }
        public List<UserProjectDto> Projects { get; set; }
    }
}