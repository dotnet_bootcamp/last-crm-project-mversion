﻿namespace CrocusoftReport.Identity.ViewModels
{
    public class UserProjectDto
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
    }
}