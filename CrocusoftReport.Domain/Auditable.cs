﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrocusoftReport.Domain
{
    public class Auditable<TUser> : BaseEntity where TUser : AppUser
    {
        [ForeignKey("created_by")]
        public int CreatedById { get; protected set; }
        public DateTime RecordDate { get; protected set; }
        public TUser CreatedBy { get; protected set; }
        public void SetAuditFields(int createdById)
        {
            if (CreatedById != 0 && CreatedById != createdById)
            {
                throw new DomainException("CreatedBy already set");
            }
            CreatedById = createdById;
            RecordDate = DateTime.UtcNow.AddHours(4);
        }
    }
}