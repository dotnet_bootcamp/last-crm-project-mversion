﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrocusoftReport.Domain
{
    public class Editable<TUser> : Auditable<TUser> where TUser : AppUser
    {
        [ForeignKey("updated_by")]
        public int? UpdatedById { get; protected set; }
        public DateTime? LastUpdateDate { get; protected set; }
        public TUser UpdatedBy { get; protected set; }
        public void SetEditFields(int? updatedById)
        {
            UpdatedById = updatedById;
            LastUpdateDate = DateTime.UtcNow.AddHours(4);
        }
    }
}