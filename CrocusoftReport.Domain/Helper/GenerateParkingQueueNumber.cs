﻿using System.Text;

namespace CrocusoftReport.Domain.Helper
{
    public class GenerateParkingQueueNumber
    {
        public static StringBuilder GetNumber(int number)
        {
            StringBuilder generatedNumber = new($"{number + 1}-{DateTime.UtcNow.DayOfYear}");

            return generatedNumber;
        }
    }
}