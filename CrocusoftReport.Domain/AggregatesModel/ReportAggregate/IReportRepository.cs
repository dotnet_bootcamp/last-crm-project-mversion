﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.ReportAggregate
{
    public interface IReportRepository : IRepository<Report>
    {
    }
}