﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.ReportAggregate
{
    public class Report : Editable<AppUser>, IAggregateRoot
    {
        public Project Project { get; private set; }
        public int ProjectId { get; private set; }
        public string Note { get; private set; }

        public void SetDetails(string note, int projectId)
        {
            Note = note;
            ProjectId = projectId;
        }
    }
}