﻿using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.UserAggregate
{
    public class UserRole : BaseEntity
    {
        public int UserId { get; private set; }

        public int? RoleId { get; private set; }

        public Role Role { get; private set; }

        public UserRole()
        {
        }

        public UserRole(int? roleId) : this()
        {
            RoleId = roleId;
        }

        public UserRole(int userId, int? roleId) : this(roleId)
        {
            UserId = userId;
            RoleId = roleId;
        }

        public void AddToInfo(int userId, int roleId)
        {
            UserId = userId;
            RoleId = roleId;
        }
    }
}