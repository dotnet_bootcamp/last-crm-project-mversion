﻿using CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.Exceptions;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.AppUserAggregate
{
    public class AppUser : BaseEntity, IAggregateRoot
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string PasswordHash { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsDeleted { get; private set; }
        public string? RefreshToken { get; private set; }
        public int RoleId { get; private set; }
        public Role Role { get; private set; }
        public int? TeamId { get; private set; }
        public Team? Team { get; private set; }
        public string? Otp { get; private set; }
        public DateTime? OtpExpiration { get; private set; }
        public List<Report> Reports { get; private set; }
        public List<ProjectUser>? ProjectUsers { get; private set; }
        public AppUser(string email, string passwordHash, string firstName, string lastName) : this()
        {
            Email = email;
            PasswordHash = passwordHash;
            FirstName = firstName;
            LastName = lastName;
            IsActive = true;
        }
        public AppUser()
        {
        }
        public void SetPasswordHash(string newPasswordHash)
        {
            if (PasswordHash != newPasswordHash)
            {
                PasswordHash = newPasswordHash;
            }
        }
        public void ResetPassword(string newPasswordHash)
        {
            if (newPasswordHash == PasswordHash)
            {
                throw new DomainException("Do not assign the same password!");
            }
            PasswordHash = newPasswordHash;
            Otp = null;
            OtpExpiration = null;
        }
        public void UpdateRefreshToken(string token)
        {
            RefreshToken = token;
        }
        public void SetDetails(string email, string firstName, string lastName)
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
        }
        public void SetRole(int roleId)
        {
            RoleId = roleId;
        }
        public void SetTeam(int? teamId)
        {
            TeamId = teamId;
        }
        public void Delete()
        {
            IsActive = false;
            IsDeleted = true;
        }
        public void SetActivated(bool isActive)
        {
            IsActive = isActive;
        }
        public void SetOtp(string otp)
        {
            Otp = otp;
            OtpExpiration = DateTime.UtcNow.AddHours(4).AddMinutes(5);
        }
    }
}