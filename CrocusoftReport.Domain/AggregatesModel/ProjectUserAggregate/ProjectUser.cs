﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate
{
    public class ProjectUser : BaseEntity
    {
        public AppUser User { get; private set; }
        public int UserId { get; private set; }
        public Project Project { get; private set; }
        public int ProjectId { get; private set; }
        public ProjectUser()
        {

        }
        public ProjectUser(int userId, int projectId)
        {
            UserId = userId;
            ProjectId = projectId;
        }
        public void SetAppUser(int userId)
        {
            UserId = userId;
        }
        public void SetProject(int projectId)
        {
            ProjectId = projectId;
        }
    }
}