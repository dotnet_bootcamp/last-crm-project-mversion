﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.RoleAggregate
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}