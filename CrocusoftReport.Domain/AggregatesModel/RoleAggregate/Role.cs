﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.RoleAggregate
{
    public class Role : BaseEntity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool IsDeleted { get; private set; }
        private readonly List<AppUser> _users;
        public IReadOnlyCollection<AppUser> Users => _users;
        public Role()
        {
            _users = new List<AppUser>();
        }
        public void SetDetails(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}