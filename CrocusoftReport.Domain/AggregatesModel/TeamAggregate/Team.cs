﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.TeamAggregate
{
    public class Team : BaseEntity, IAggregateRoot
    {
        public string TeamName { get; private set; }
        private readonly List<AppUser>? _users;
        public IReadOnlyCollection<AppUser>? Users => _users;
        public Team()
        {
            _users = new List<AppUser>();
        }
        public void SetDetails(string name)
        {
            TeamName = name;
        }
    }
}