﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;

namespace CrocusoftReport.Domain.AggregatesModel.TeamAggregate
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
}