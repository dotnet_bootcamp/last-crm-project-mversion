﻿using Autofac;
using AutoMapper;
using CrocusoftReport.Domain.AggregatesModel.ProjectAggregate;
using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Domain.AggregatesModel.UserAggregate;
using CrocusoftReport.Identity;
using CrocusoftReport.Identity.Auth;
using CrocusoftReport.Infrastructure.Idempotency;
using CrocusoftReport.Infrastructure.Identity;
using CrocusoftReport.Infrastructure.Repositories;
using CrocusoftReport.ProjectDetails;
using CrocusoftReport.ReportDetails;
using CrocusoftReport.ReportDetails.Queries;
using CrocusoftReport.SharedKernel.Infrastructure.Helper;
using CrocusoftReport.TeamDetails;
using CrocusoftReport.UserDetails;

namespace CrocusoftReport.Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutofacHelper.RegisterCqrsTypes<ApplicationModule>(builder);
            AutofacHelper.RegisterCqrsTypes<IdentityModule>(builder);
            AutofacHelper.RegisterCqrsTypes<UserModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ProjectModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ReportModule>(builder);
            AutofacHelper.RegisterCqrsTypes<TeamModule>(builder);
            // Services

            // Repositories
            builder.RegisterType<ClaimsManager>()
                .As<IClaimsManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserManager>()
                .As<IUserManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<NoteService>()
              .AsSelf();

            builder.RegisterType<SendEmailAsync>()
                .As<ISendEmailAsync>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AppUserRepository>()
                .As<IAppUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProjectRepository>()
                .As<IProjectRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TeamRepository>()
              .As<ITeamRepository>()
              .InstancePerLifetimeScope();

            builder.RegisterType<ReportRepository>()
           .As<IReportRepository>()
           .InstancePerLifetimeScope();

            // AutoMapper
            AutofacHelper.RegisterAutoMapperProfiles<ApplicationModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<IdentityModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<UserModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ProjectModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<TeamModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ReportModule>(builder);

            builder.Register(ctx =>
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    foreach (var profile in ctx.Resolve<IList<Profile>>()) cfg.AddProfile(profile);
                });
                return mapperConfiguration.CreateMapper();
            })
                .As<IMapper>()
                .SingleInstance();
        }
    }
}