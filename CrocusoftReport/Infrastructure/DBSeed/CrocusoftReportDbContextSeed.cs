﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using System.Data.SqlClient;

namespace CrocusoftReport.Infrastructure.DBSeed
{
    public class CrocusoftReportDbContextSeed
    {
        public async Task SeedAsync(
            CrocusoftReportDbContext context,
            IWebHostEnvironment env,
            IOptions<CrocusoftReportSettings> settings,
            ILogger<CrocusoftReportDbContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(CrocusoftReportDbContextSeed));
            using (context)
            {
                await policy.ExecuteAsync(async () =>
                {
                    await context.SaveChangesAsync();
                    var systemUser = await context.Users.FirstOrDefaultAsync();
                    if (systemUser == null)
                    {
                        var superAdminRole = new Role();
                        superAdminRole.SetDetails(RoleParametr.SuperAdmin.Name, "Super Admin");
                        await context.Roles.AddAsync(superAdminRole);
                        var adminRole = new Role();
                        adminRole.SetDetails(RoleParametr.Admin.Name, "Admin");
                        await context.Roles.AddAsync(adminRole);
                        var humanResourceRole = new Role();
                        var headRole = new Role();
                        headRole.SetDetails(RoleParametr.Head.Name, "Head");
                        await context.Roles.AddAsync(headRole);
                        var userRole = new Role();
                        userRole.SetDetails(RoleParametr.Employee.Name, "Employee");
                        await context.Roles.AddAsync(userRole);
                        var merchantRole = new Role();
                        await context.SaveChangesAsync();
                        var user = new AppUser("marketing@crocusoft.com", PasswordHasher.HashPassword("sdfsdfsdf@"), "Melek", "Haciahmedova");
                        user.SetRole(superAdminRole.Id);
                        await context.Users.AddAsync(user);
                        await context.SaveChangesAsync();
                    }
                });
            }
        }
        private AsyncRetryPolicy CreatePolicy(ILogger<CrocusoftReportDbContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().WaitAndRetryAsync(
                retries,
                retry => TimeSpan.FromSeconds(5),
                (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogTrace(
                        $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                }
            );
        }
    }
}