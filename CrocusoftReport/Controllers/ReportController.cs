﻿using CrocusoftReport.CustomAuthorizeLogic;
using CrocusoftReport.Extensions;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.ReportDetails;
using CrocusoftReport.ReportDetails.Commands;
using CrocusoftReport.ReportDetails.Queries;
using CrocusoftReport.ReportDetails.ViewModels;
using CrocusoftReport.SharedKernel.Domain.Seedwork;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Controllers
{
    [Route("report")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IReportQueries _reportQueries;
        public ReportController(IMediator mediator, IReportQueries reportQueries)
        {
            _mediator = mediator;
            _reportQueries = reportQueries;
        }
        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateReportCommand, bool>(command, requestId);
            return Ok(new { Message = "Report created successfully!" });
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateReportCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateReportCommand, bool>(command, requestId);
            return Ok();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpGet("admin")]
        public async Task<AllReportDto> GetAllAdminAsync([FromQuery] ReportRequestForAdminDto request, [FromQuery] LoadMoreDto dto)
        {
            return await _reportQueries.GetAllAdminAsync(request, dto);
        }

        [AuthorizeRoles(CustomRole.Employee)]
        [HttpGet("user")]
        public async Task<AllReportDto> GetAllUserAsync([FromQuery] ReportRequestForUserDto request, [FromQuery] LoadMoreDto dto)
        {
            return await _reportQueries.GetAllUserAsync(request, dto);
        }

        [HttpGet("{id}")]
        public async Task<ReportDto> GetByIdAsync([FromRoute] int id)
        {
            return await _reportQueries.GetByIdAsync(id);
        }

        [HttpGet("export-to-excel")]
        public async Task<IActionResult> ExportToExcel([FromQuery] ReportRequestForAdminDto request, [FromQuery] LoadMoreDto dto)
        {
            try
            {
                var reports = await _reportQueries.GetAllAdminAsync(request, dto);

                if (reports == null || reports.Reports == null || reports.Reports.Count == 0)
                {
                    return NotFound("No reports found for export.");
                }
                var excelBytes = ExportToExcelService.ExportToExcel(reports.Reports);
                return File(excelBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reports.xlsx");
            }
            catch (Exception)
            {
                // Log the exception
                return StatusCode(500, "An error occurred while exporting reports.");
            }
        }
    }
}