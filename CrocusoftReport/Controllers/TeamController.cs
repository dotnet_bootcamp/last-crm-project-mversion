﻿using CrocusoftReport.Extensions;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.TeamDetails.Commands;
using CrocusoftReport.TeamDetails.Queries;
using CrocusoftReport.TeamDetails.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Controllers
{
    [Route("team")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ITeamQueries _teamQueries;
        public TeamController(IMediator mediator, ITeamQueries teamQueries)
        {
            _mediator = mediator;
            _teamQueries = teamQueries;
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        public async Task<AllTeamDto> GetAllAsync([FromQuery] LoadMoreDto loadMore)
        {
            return await _teamQueries.GetAllAsync(loadMore);
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("{id}")]
        public async Task<TeamGetByIdDto> GetByIdAsync([FromRoute] int id)
        {
            return await _teamQueries.GetByIdAsync(id);
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateTeamCommand, bool>(command, requestId);
            return Ok();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpDelete("hard")]
        public async Task<IActionResult> Delete([FromBody] DeleteTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteTeamCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}