﻿using CrocusoftReport.Identity.Queries;
using CrocusoftReport.Identity.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Controllers
{
    [Route("role")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleQueries _roleQueries;

        public RoleController(IRoleQueries roleQueries)
        {
            _roleQueries = roleQueries;
        }

        [HttpGet]
        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            return await _roleQueries.GetAllAsync();
        }
    }
}