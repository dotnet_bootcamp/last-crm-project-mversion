﻿using CrocusoftReport.Extensions;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.ProjectDetails.Commands;
using CrocusoftReport.ProjectDetails.Queries;
using CrocusoftReport.ProjectDetails.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CrocusoftReport.Controllers
{
    [Route("project")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProjectQueries _projectQueries;
        public ProjectController(IMediator mediator, IProjectQueries projectQueries)
        {
            _mediator = mediator;
            _projectQueries = projectQueries;
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateProjectCommand, bool>(command, requestId);
            return NoContent();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateProjectCommand, bool>(command, requestId);
            return Ok();
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        public async Task<AllProjectDto> GetAllAsync([FromQuery] ProjectRequestDto request, [FromQuery] LoadMoreDto dto)
        {
            return await _projectQueries.GetAllAsync(request, dto);
        }

        //[AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("{id}")]
        public async Task<ProjectGetByIdDto> GetByIdAsync([FromRoute] int id)
        {
            return await _projectQueries.GetByIdAsync(id);
        }
    }
}