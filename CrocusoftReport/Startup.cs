﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using CrocusoftReport.Infrastructure;
using CrocusoftReport.Infrastructure.AutofacModules;
using CrocusoftReport.Infrastructure.Constants;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.Infrastructure.ErrorHandling;
using CrocusoftReport.Infrastructure.Filters;
using CrocusoftReport.Infrastructure.Identity;
using CrocusoftReport.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace CrocusoftReport
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }
        public IWebHostEnvironment Environment { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder.AllowAnyOrigin()
            //            .AllowAnyMethod()
            //            .AllowAnyHeader());
            //});
            services.AddControllers();
            services.AddControllers()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.Error = (_, args) =>
                    {
                        Console.WriteLine(args.ErrorContext.Error);
                    };
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                }
                );
            var settings = Configuration.Get<CrocusoftReportSettings>();
            services.Configure<CrocusoftReportSettings>(Configuration);
            services.Configure<SharedKernel.Infrastructure.EmailConfiguration>(Configuration.GetSection("EmailConfiguration"));
            services.Configure<SmsOptions>(Configuration.GetSection(SmsOptions.Sms));

            if (Environment.IsDevelopment())
            {
                services.Configure<CrocusoftReportFileSettings>(Configuration.GetSection(CrocusoftReportFileSettings.BasePathWindows));
            }
            else
            {
                services.Configure<CrocusoftReportFileSettings>(Configuration.GetSection(CrocusoftReportFileSettings.BasePathLinux));
            }

            // Add framework services.
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
                .AddControllersAsServices();
            services.AddLogging();
            services.AddEntityFrameworkSqlServer()
               .AddDbContext<CrocusoftReportDbContext>(options =>
               {
                   options.UseSqlServer(settings.DefaultConnection, /*ServerVersion.AutoDetect(settings.DefaultConnection),*/
                         sqlOptions =>
                         {
                             sqlOptions.MigrationsAssembly("CrocusoftReport.Infrastructure.Migrations");
                         });
               });
            ConfigureSwagger(services);
            ConfigureJwtAuthentication(services, settings);

            // Add application services.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddOptions();
            services.AddHttpContextAccessor();
            services.AddScoped<IClaimsManager, ClaimsManager>();

            //configure Autofac
            var container = new ContainerBuilder();
            container.Populate(services);


            //container.RegisterType<SendEmailAsync>().AsSelf();
            //    ;

            container.RegisterModule(new MediatorModule());
            container.RegisterModule(new ApplicationModule());
            return new AutofacServiceProvider(container.Build());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", true, true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
              .AddEnvironmentVariables();
            Configuration = builder.Build();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseMiddleware<AuthorizationErrorMiddleware>();
            app.UseSerilogRequestLogging();
            app.UseHttpLogging();
            app.UseSwagger()
              .UseSwaggerUI(c =>
              {
                  c.SwaggerEndpoint("/swagger/v1/swagger.json", "CrocusoftReport.API V1");

                  c.DocumentTitle = "CrocusoftReport API";
                  c.DocExpansion(DocExpansion.List);
              });

            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
        #region HelperMethods

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "CRM API",
                    Version = "v1",
                    Description = "CRM Service API"
                });

                options.OperationFilter<AuthorizeCheckOperationFilter>();
                options.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: bearer {token}\"",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    In = ParameterLocation.Header,
                    BearerFormat = "JWT",
                    Scheme = "bearer"
                });
            });
        }

        private void ConfigureJwtAuthentication(IServiceCollection services, CrocusoftReportSettings settings)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;   
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = settings.JwtIssuer,
                        ValidateAudience = true,
                        ValidAudience = settings.JwtIssuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.JwtKey)),
                        ClockSkew = TimeSpan.FromSeconds(15),
                        ValidateLifetime = true
                    };
                });
        }
        #endregion
    }
}