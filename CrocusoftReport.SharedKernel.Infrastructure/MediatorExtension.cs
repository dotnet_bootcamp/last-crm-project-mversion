﻿using CrocusoftReport.SharedKernel.Domain.Seedwork;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.SharedKernel.Infrastructure
{
    public static class MediatorExtension
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, DbContext ctx)
        {
            var domainEntities = ctx.ChangeTracker
                .Entries<BaseEntity>()
                .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
                .ToList();
            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();
            domainEntities.ForEach(entity => entity.Entity.DomainEvents.Clear());
            var tasks = domainEvents.Select(async @event => await mediator.Publish(@event));
            await Task.WhenAll(tasks);
        }
    }
}