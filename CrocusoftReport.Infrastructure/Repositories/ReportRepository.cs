﻿using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Repositories
{
    public class ReportRepository : Repository<Report>, IReportRepository
    {
        public sealed override DbContext Context { get; protected set; }
        public ReportRepository(CrocusoftReportDbContext context)
        {
            Context = context;
        }
    }
}