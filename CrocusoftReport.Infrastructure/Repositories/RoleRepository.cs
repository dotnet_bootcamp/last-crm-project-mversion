﻿using CrocusoftReport.Domain.AggregatesModel.RoleAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public RoleRepository(CrocusoftReportDbContext context)
        {
            Context = context;
        }
    }
}
