﻿using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using CrocusoftReport.Infrastructure.Database;
using CrocusoftReport.SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace CrocusoftReport.Infrastructure.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public sealed override DbContext Context { get; protected set; }
        public TeamRepository(CrocusoftReportDbContext context)
        {
            Context = context;
        }
    }
}