﻿using CrocusoftReport.Domain.AggregatesModel.TeamAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrocusoftReport.Infrastructure.EntityConfigurations.TeamEntityConfiguration
{
    public class TeamEntityTypeConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> teamConfiguration)
        {
            teamConfiguration.ToTable("teams");
            teamConfiguration.HasKey(t => t.Id);
            teamConfiguration.Property(t => t.TeamName).IsRequired().HasMaxLength(20);
        }
    }
}