﻿using CrocusoftReport.Domain.AggregatesModel.ProjectUserAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrocusoftReport.Infrastructure.EntityConfigurations.ProjectUserEntityConfiguration
{
    public class ProjectUserEntityTypeConfiguration : IEntityTypeConfiguration<ProjectUser>
    {
        public void Configure(EntityTypeBuilder<ProjectUser> projectUserConfiguration)
        {
            projectUserConfiguration.ToTable("user_projects");
            projectUserConfiguration.HasKey(pu => pu.Id);
            projectUserConfiguration.HasOne(pu => pu.User)
                .WithMany(pu => pu.ProjectUsers)
                .HasForeignKey(pu => pu.UserId);
            projectUserConfiguration.HasOne(pu => pu.Project)
                .WithMany(pu => pu.ProjectUsers)
                .HasForeignKey(pu => pu.ProjectId);
        }
    }
}