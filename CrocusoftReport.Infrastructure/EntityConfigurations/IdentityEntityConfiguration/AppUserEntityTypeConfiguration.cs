﻿using CrocusoftReport.Domain.AggregatesModel.AppUserAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrocusoftReport.Infrastructure.EntityConfigurations.IdentityEntityConfiguration
{
    public class AppUserEntityTypeConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> userConfiguration)
        {
            userConfiguration.ToTable("users");
            userConfiguration.HasKey(o => o.Id);
            userConfiguration.Property(o => o.Id).HasColumnName("id");
            userConfiguration.Property(o => o.FirstName).HasMaxLength(100).IsRequired().HasColumnName("first_name");
            userConfiguration.Property(o => o.Email).HasMaxLength(100).IsRequired().HasColumnName("email");
            userConfiguration.Property(o => o.PasswordHash).IsRequired().HasColumnName("password_hash");
            userConfiguration.Property(o => o.RefreshToken).HasColumnName("refresh_token");
            userConfiguration.Property(o => o.LastName).HasColumnName("last_name");
            userConfiguration.Property(o => o.RoleId).HasColumnName("role_id");
            userConfiguration.Property(o => o.TeamId).HasColumnName("team_id");
            userConfiguration.Property(o => o.IsActive).HasColumnName("is_active");
            userConfiguration.Property(o => o.Otp).HasColumnName("otp");
            userConfiguration.Property(o => o.OtpExpiration).HasColumnName("otp-expiration");
            userConfiguration.HasOne(e => e.Role)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.RoleId);
            userConfiguration.HasOne(e => e.Team)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.TeamId);
        }
    }
}