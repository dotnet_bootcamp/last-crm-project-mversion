﻿using CrocusoftReport.Domain.AggregatesModel.ReportAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrocusoftReport.Infrastructure.EntityConfigurations.ReportEntityConfiguration
{
    public class ReportEntityTypeConfiguration : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> reportConfiguration)
        {
            reportConfiguration.ToTable("reports");
            reportConfiguration.HasKey(r => r.Id);
            reportConfiguration.Property(r => r.Note).HasMaxLength(400);
            reportConfiguration.HasOne(r => r.Project)
                .WithMany(r => r.Reports)
                .HasForeignKey(r => r.ProjectId);
            reportConfiguration.HasOne(r => r.CreatedBy)
            .WithMany()
            .HasForeignKey(r => r.CreatedById);
        }
    }
}