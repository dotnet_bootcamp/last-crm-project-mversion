using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CrocusoftReport.Infrastructure.Database
{
    public class CrocusoftReportDbContextDesignFactory : IDesignTimeDbContextFactory<CrocusoftReportDbContext>
    {
        public CrocusoftReportDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CrocusoftReportDbContext>()
                   .UseSqlServer("Data Source=SQL5107.site4now.net;Initial Catalog=db_aa4f50_crocusoftcrm;User Id=db_aa4f50_crocusoftcrm_admin;Password=Liminal221space", sqlOptions =>
                   {
                       sqlOptions.MigrationsAssembly("CrocusoftReport.Infrastructure.Migrations");
                   });
            return new CrocusoftReportDbContext(optionsBuilder.Options, new NoMediator());
        }
        private class NoMediator : IMediator
        {
            public Task Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = default) where TNotification : INotification
            {
                return Task.CompletedTask;
            }
            public Task Publish(object notification, CancellationToken cancellationToken = default)
            {
                return Task.CompletedTask;
            }
            public Task<TResponse> Send<TResponse>(IRequest<TResponse> request,
                CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.FromResult(default(TResponse));
            }
            public Task Send(IRequest request, CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.CompletedTask;
            }
            public Task<object> Send(object request, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }
        }
    }
}